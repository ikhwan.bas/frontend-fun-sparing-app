import { createContext, useState } from "react";

export const AdminContext = createContext();

export const AdminProvider = (props) => {
    // to check current Admin when browser was refreshed
    const currentAdmin = JSON.parse(localStorage.getItem('admin'));

    // state for admin
    const [admin, setAdmin] = useState(currentAdmin);

    return (
        <AdminContext.Provider value={[admin, setAdmin]}>
            {props.children}
        </AdminContext.Provider>
    )
}