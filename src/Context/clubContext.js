import { createContext, useState } from "react";

export const ClubContext = createContext();

export const ClubProvider = (props) => {
    // to check current Club when browser was refreshed
    const currentClub = JSON.parse(localStorage.getItem('club'));

    // state for club
    const [club, setClub] = useState(currentClub);

    return (
        <ClubContext.Provider value={[club, setClub]}>
            {props.children}
        </ClubContext.Provider>
    )
}