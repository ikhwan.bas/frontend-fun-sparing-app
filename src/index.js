import 'bootstrap/dist/css/bootstrap.min.css';

import './Style/index.css';

import React from 'react';
import ReactDOM from 'react-dom/client';
import AppRoutes from './Routes/Routes';
import { AdminProvider } from './Context/adminContext';
import { ClubProvider } from './Context/clubContext';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <AdminProvider>
      <ClubProvider>
        <AppRoutes />
      </ClubProvider>
    </AdminProvider>
  </React.StrictMode>
);

