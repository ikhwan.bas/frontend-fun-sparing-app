import "../../Style/venue.css"
import { Card, Col, Row } from "react-bootstrap"
import imageBottom from "../../Images/background.jpg"
import { useEffect, useState } from "react"
import axios from "axios"
import { useNavigate, useParams } from "react-router-dom"

let apiURL = "https://fun-sparing.herokuapp.com"

const VenueDetail = () => {
    const [venue, setVenue] = useState([])
    const [venuedetail, setVenuedetail] = useState([])
    const [category, setCategory] = useState('')
    let { id } = useParams()
    let navigate = useNavigate()
    useEffect(() => {
        const fetchData = async () => {
            const venue = await axios.get(`${apiURL}/venues`)
            const venueResult = venue.data.venue
            setVenue(venueResult)

            const venueDetail = await axios.get(`${apiURL}/venues/${id}`)
            const venueDetailRes = venueDetail.data.data[0]
            setVenuedetail(venueDetailRes)
            setCategory(venueDetailRes.SportCategory.category)
        }
        fetchData()
    }, [id])

    return (
        <div className="homepage">
            <div className="content-container padding-top-5">
                <div className="header-tag display-center background-color text-white margin-lr-tb-7 border-radius20">
                    <h2>#Temukan lapangan {category} pilihanmu</h2>
                </div>
                <div className="flex-top margin-lr-tb border-test border-radius20">
                    <div className="venue-detail-info text-center padding-lrtb-percent3">
                        <h1 className="text-color">{venuedetail.venue_name}</h1>
                        <br />

                        <img src={venuedetail.image_url} alt="futsal-images" />
                    </div>

                    <div className="additional-info padding-lrtb-percent5 border-test-1 border-radius20 background-color text-white">
                        <h4>Kategori Olahraga : </h4>
                        <h6>  {category}</h6>


                        <br />
                        <h4>Fasilitas :</h4>
                        <h6>{venuedetail.facility}</h6>
                        <br />

                        <h4>Kab / Kota :</h4>
                        <h6>{venuedetail.city}</h6>
                        <br />

                        <h4>Provinsi :</h4>
                        <h6>{venuedetail.province}</h6>
                        <br />
                        <h4>Harga Sewa :</h4>
                        <br />
                        <div className="padding-lrtb-percent border-radius20 text-color background-white text-center">
                            <h4>Rp. {venuedetail.price} per jam </h4>

                        </div>
                    </div>
                </div>

                <div className="home-event-container display-center margin-lr-tb padding-lrtb-percent3 border-radius20">
                    <h2 className="text-white">Pilihan lapangan {category} lainnya :</h2>
                    <Row xs={1} md={4} className="g-4 card-container">
                        {venue.map((item) => {
                            return (
                                <Col>
                                    <Card onClick={() => { navigate(`/venue/${item.id}`) }}>
                                        <Card.Img variant="top" src={item.image_url} />
                                        <Card.Body>
                                            <Card.Title>{item.venue_name}</Card.Title>
                                            <Card.Text>
                                                {item.facility}
                                            </Card.Text>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            )
                        })}
                    </Row>
                </div>

                {/* image-bottom */}
                <div className="home-image-container display-center ">
                    <h2>Nyalakan Semangatmu, Cari Kawan Main Disini</h2>
                    <img src={imageBottom} alt="futsal-images" />
                </div>
            </div>
        </div>

    )
}

export default VenueDetail