import "../../Style/event.css"
import { Alert, Button, Card, Col, Row, Table } from "react-bootstrap"
import imageBottom from "../../Images/background.jpg"
import { useContext, useEffect, useState } from "react"
import axios from "axios"
import { useNavigate, useParams } from "react-router-dom"
import { ClubContext } from "../../Context/clubContext"

let apiURL = "https://fun-sparing.herokuapp.com"

const EventDetail = () => {
    const [club,] = useContext(ClubContext)
    let { id } = useParams()

    const [input, setInput] = useState({ event_id: id })
    const [error, setError] = useState()
    const [event, setEvent] = useState([])
    const [eventdetail, setEventdetail] = useState([])
    const [cart, setCart] = useState([])

    const [category, setCategory] = useState('')
    const [venue, setVenue] = useState('')

    let navigate = useNavigate()

    useEffect(() => {
        const fetchData = async () => {
            // get all events data
            const event = await axios.get(`${apiURL}/events`)
            const eventResult = event.data.events
            setEvent(eventResult)

            // get selected event data
            const eventdetail = await axios.get(`${apiURL}/events/${id}`)
            const eventdetailRes = eventdetail.data.data[0]
            setEventdetail(eventdetailRes)
            setCategory(eventdetailRes.SportCategory.category)
            setVenue(eventdetailRes.Venue.venue_name)

            // get event cart
            const carts = await axios.get(`${apiURL}/events/cart/${id}`)
            setCart(carts.data.data);
        }
        fetchData()
    }, [id])

    const handleClick = async (event) => {
        if (club === null) {
            setError("Please login first")
        } else {
            event.preventDefault()
            await axios.post(`${apiURL}/events/cart`, input, { headers: { Authorization: "Bearer " + club.token } })
                .then((res) => {
                    window.location.reload()
                    setInput({})
                })
                .catch((err) => {
                    setError(err.response.data.error)
                })
        }
    }

    return (
        <div className="homepage">
            <div className="content-container padding-top5">
                <div className="header-tag display-center background-color text-white margin-lr-tb-7 border-radius20">
                    <h2># # # Turnamen {category} # # #</h2>
                </div>
                {error !== undefined && (
                    <Alert className="margin-lr-tb" key="danger" variant="danger">{error}</Alert>
                )}
                <div className="event-detail-container flex-top margin-lr-tb border-test border-radius20">
                    <div className="event-detail-info text-center">
                        <img src={eventdetail.poster_url} alt="futsal-images" />
                    </div>
                    <div className="event-additional-info padding-lrtb-percent3">

                        <h1>{eventdetail.event_name}</h1>
                        <br />
                        <h5> Lokasi : {eventdetail.city}, {eventdetail.province}</h5>
                        <br />
                        <h5> Deskripsi Kegiatan : </h5>
                        <h6> {eventdetail.description}</h6>
                        <br />
                        <h5> Jadwal Kegiatan : </h5>
                        <h6> {eventdetail.time}</h6>
                        <br />
                        <Button onClick={handleClick} className="button-add-product" type="submit">
                            Daftar
                        </Button>
                    </div>
                </div>
                <div className="event-detail-container flex-top margin-lr-tb border-radius20">
                    <div className="event-team-join padding-lrtb-percent3 border-radius20 border-test">
                        <h4>Club Bergabung :</h4>
                        <br />
                        {cart.length === 0 && (
                            <>
                                <h6>Oops, Belum ada club yang bergabung .</h6>
                                <h6>Daftarkan clubmu sekarang</h6>
                                <Button onClick={handleClick} className="button-add-product" type="submit">
                                    Daftar
                                </Button>
                            </>
                        )}
                        {
                            cart.map((item) => {
                                return (
                                    <div className="card-container flex-top padding-lrtb-percent3 border-test border-radius20">
                                        <div className="card-image-container">
                                            <img src={item.Club.logo_url} alt="logo_club" />
                                        </div>
                                        <div className="card-content-container">
                                            <Table >
                                                <thead>
                                                    <tr>
                                                        <th>Nama Club</th>
                                                        <td>{item.Club.club_name}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Asal</th>
                                                        <td>{item.Club.city},{item.Club.province} </td>
                                                    </tr>
                                                </thead>
                                            </Table>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>

                    <div className="event-host padding-lrtb-percent3 border-radius20 border-test background-color text-white">
                        <h4>INFO HOST</h4>
                        <h6>{eventdetail.host}</h6>

                        <h4>Email :</h4>
                        <h6>{eventdetail.email}</h6>

                        <h4>Nomor Telepon :</h4>
                        <h6>{eventdetail.phone}</h6>

                        <h4>Venue :</h4>
                        <h6>{venue}</h6>
                        <h4>Biaya Pendaftaran :</h4>
                        <div className="padding-lrtb-percent border-radius20 text-color background-white text-center">
                            <h4>Rp. {eventdetail.fee} per team </h4>

                        </div>

                    </div>
                </div>

                <div className="home-event-container display-center margin-lr-tb padding-lrtb-percent3">
                    <h2 className="text-white">Pilihan turnamen {category} lainnya :</h2>
                    <Row xs={1} md={4} className="g-4 card-container">
                        {event.map((item) => {
                            return (
                                <Col>
                                    <Card onClick={() => { navigate(`/event/${item.id}`) }}>
                                        <Card.Img variant="top" src={item.poster_url} />
                                        <Card.Body>
                                            <Card.Title>{item.event_name}</Card.Title>
                                            <Card.Text>
                                                {item.description}
                                            </Card.Text>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            )
                        })}
                    </Row>
                </div>

                {/* image-bottom */}
                <div className="home-image-container display-center ">
                    <h2>Nyalakan Semangatmu, Cari Kawan Main Disini</h2>
                    <img src={imageBottom} alt="futsal-images" />
                </div>
            </div>
        </div>

    )
}

export default EventDetail