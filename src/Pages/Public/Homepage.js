import { Card, Col, Row } from "react-bootstrap"
import CarouselComp from "../../Components/Carousel/Carousel"
import logo from "../../Images/logo.jpg"
import imageBottom from "../../Images/background.jpg"
import { useEffect, useState } from "react"
import axios from "axios"
import { useNavigate } from "react-router-dom"

let apiURL = "https://fun-sparing.herokuapp.com"

const Homepage = () => {
    const [venue, setVenue] = useState([])
    const [event, setEvent] = useState([])

    let navigate = useNavigate()

    useEffect(() => {
        const fetchData = async () => {
            const venue = await axios.get(`${apiURL}/venues`)
            const result = venue.data.venue
            setVenue(result)

            const event = await axios.get(`${apiURL}/events`)
            const eventResult = event.data.events
            setEvent(eventResult)
        }
        fetchData()
    }, [])


    return (
        <div className="homepage">
            <div className="content-container">
                <CarouselComp />
                <div className="display-center home-about-us margin-lrtb">
                    <h2 className="text-color">Tentang Fun Sparing</h2>
                    <div className="logo-container">
                        <img src={logo} alt="logo" />
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit exercitationem excepturi doloremque dolorum quae, laboriosam reprehenderit obcaecati autem ratione cum deserunt recusandae voluptate praesentium architecto quasi natus id totam labore provident nam unde! Earum unde adipisci pariatur? Repellat, dolore culpa!</p>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Doloremque dolore eos laboriosam esse amet! Ex ea corrupti, quidem facere culpa expedita rem repellendus molestiae dolorum fuga harum eveniet a deserunt saepe. Fugiat qui cumque quos dolorem illum consequatur tempore, quam, nemo suscipit culpa corporis iure, perspiciatis dignissimos enim dolorum quasi.</p>
                    </div>

                </div>
                <div className="home-event-container display-center padding-lrtb">
                    <h2 className="text-white">Event Terkini</h2>
                    <Row xs={1} md={4} className="g-4 card-container">
                        {event.map((item) => {
                            return (
                                <Col>
                                    <Card onClick={() => { navigate(`/event/${item.id}`) }}>
                                        <Card.Img variant="top" src={item.poster_url} />
                                        <Card.Body>
                                            <Card.Title>{item.event_name}</Card.Title>
                                            <Card.Text>
                                                {item.description}
                                            </Card.Text>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            )
                        })}
                    </Row>
                </div>

                {/* Lapangan Futsal */}
                <div className="home-field-container display-center padding-lrtb">
                    <h2 className="text-color">Lapangan Tersedia</h2>
                    <Row xs={1} md={2}>
                        {venue.map((venue) => (
                            <Col>
                                <div className="create-card" onClick={() => { navigate(`/venue/${venue.id}`) }}>
                                    <div className="create-card-image">
                                        <img src={venue.image_url} alt={venue.id} />
                                    </div>
                                    <div className="card-content">
                                        <h5>Venue</h5>

                                        <h6>{venue.venue_name}</h6>
                                        <h6>{venue.city}</h6>
                                        <h6>{venue.province}</h6>
                                        <h6>{venue.price}</h6>
                                    </div>
                                </div>
                            </Col>
                        ))}
                    </Row>
                </div>

                {/* image-bottom */}
                <div className="home-image-container display-center">
                    <h2>Nyalakan Semangatmu, Cari Kawan Main Disini</h2>
                    <img src={imageBottom} alt="futsal-images" />
                </div>
            </div>
        </div>

    )
}

export default Homepage