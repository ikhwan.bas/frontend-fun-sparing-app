import "../../Style/event.css"
import { Button, Card, Col, Form, Row } from "react-bootstrap"
import imageBottom from "../../Images/background.jpg"
import { useEffect, useState } from "react"
import axios from "axios"
import { useNavigate } from "react-router-dom"

let apiURL = "https://fun-sparing.herokuapp.com"

const VenuePage = () => {
    const [venue, setVenue] = useState([])
    const [category, setCategory] = useState([])
    const [input, setInput] = useState({})
    let navigate = useNavigate()

    useEffect(() => {
        const fetchData = async () => {
            const category = await axios.get(`${apiURL}/sports/category`)
            const categoryResult = category.data.categories
            setCategory(categoryResult)

            const venue = await axios.get(`${apiURL}/venues`)
            const venueResult = venue.data.venue
            setVenue(venueResult)
        }
        fetchData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name
        if (name === "fee") {
            value = parseInt(value)
        } else if (name === "phone") {
            value = parseInt(value)
        }
        setInput({ ...input, [name]: value })
    }



    return (
        <div className="homepage">
            <div className="content-container">
                <div className="banner-image-container padding-lr-tb ">
                    <img src={imageBottom} alt="futsal-images" />
                </div>
                <div className="display-center text-color ">
                    <h1>#Temukan Lapangan Pilihanmu</h1>
                </div>
                <div className="display-center margin-lr-tb border-test">
                    <Form className="display-center form-event-container">
                        <Form.Group className="form-width padding-lrtb-percent3" >
                            <Form.Control name="event_name" onChange={handleChange} type="text" placeholder="Nama Kota" />
                        </Form.Group>

                        <Form.Group className="padding-lrtb-percent3">
                            <Form.Control name="city" onChange={handleChange} type="text" placeholder="Nama Provinsi" />
                        </Form.Group>

                        <Form.Group className="padding-lrtb-percent3">
                            <Form.Select onChange={handleChange} name="sport_category_id" aria-label="Default select example">
                                <option>Pilih Jenis Olahraga</option>
                                {category.map((cat) => {
                                    return (
                                        <>
                                            <option value={cat.id}>{cat.category}</option>
                                        </>)
                                })}
                            </Form.Select>
                        </Form.Group>

                        <Button onClick={handleSubmit} className="button-add-product" type="submit">
                            Search
                        </Button>
                    </Form>
                </div>
                <div className="home-event-container display-center margin-lr-tb padding-lrtb-percent3">
                    <Row xs={1} md={4} className="g-4 card-container">
                        {venue.map((item) => {
                            return (
                                <Col>
                                    <Card onClick={() => { navigate(`/venue/${item.id}`) }}>
                                        <Card.Img variant="top" src={item.image_url} />
                                        <Card.Body>
                                            <Card.Title>{item.venue_name}</Card.Title>
                                            <Card.Text>
                                                {item.facility}
                                            </Card.Text>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            )
                        })}
                    </Row>
                </div>

                {/* image-bottom */}
                <div className="home-image-container display-center ">
                    <h2>Nyalakan Semangatmu, Cari Kawan Main Disini</h2>
                    <img src={imageBottom} alt="futsal-images" />
                </div>
            </div>
        </div>

    )
}

export default VenuePage