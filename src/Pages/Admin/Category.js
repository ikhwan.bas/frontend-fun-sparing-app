import "../../Style/dashboard.css"

import { Alert, Button, Form, Table } from "react-bootstrap"
import SidebarComp from "../../Components/Sidebar/SidebarAdmin"
import { useContext, useEffect, useState } from "react"

import { AdminContext } from "../../Context/adminContext"
import axios from "axios"

let apiURL = "https://fun-sparing.herokuapp.com"

const Category = () => {

    const [admin,] = useContext(AdminContext)
    const [input, setInput] = useState({ category: "" })
    const [error, setError] = useState()
    const [fetchTrigger, setFetchTrigger] = useState(true);
    const [data, setData] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            const category = await axios.get(`${apiURL}/sports/category`)
            const result = category.data.categories
            setData(result)
        }
        if (fetchTrigger) {
            fetchData()
            setFetchTrigger(false)
        }
    }, [fetchTrigger])

    const handleSubmit = async (event) => {
        event.preventDefault()
        await axios.post(`${apiURL}/sports/category`, input, { headers: { Authorization: "Bearer " + admin.token } }
        )
            .then((res) => {
                setFetchTrigger(true);
                setInput({ category: "" })
            })
            .catch((err) => {
                setError(err.response.data.error)
            })
    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name
        setInput({ ...input, [name]: value })
    }


    return (

        <div className="admin-content-container flex">
            <div className="dashboard-content-container flex-top">
                <SidebarComp />

                <div className="table-container margin-lrtb-percent3">
                    <div className="table-content-container">
                        <h3>Kategori Olahraga</h3>
                        {error !== undefined && (
                            <Alert key="danger" variant="danger">{error}</Alert>
                        )}
                        <br />
                        <Form>
                            <Form.Group className="mb-4 form-category" >
                                <Form.Control id="form-category" name="category" onChange={handleChange} type="text" value={input.category} placeholder="Masukkan Kategori Baru" required />
                            </Form.Group>
                            <Button onClick={handleSubmit} type="submit" variant="success">
                                Tambah Kategori
                            </Button>
                        </Form>

                        <hr />

                        <br />
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Kategori</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
                                {data.map((item, index) => {
                                    return (
                                        <tr>
                                            <td>{index + 1}</td>
                                            <td>{item.category}</td>
                                            <td>
                                                <Button name="post" id='edit-button' value='a' variant="success" size="sm">
                                                    Edit
                                                </Button>
                                            </td>
                                        </tr>
                                    )
                                })}


                            </tbody>
                        </Table>
                    </div>

                </div>
            </div>
        </div>


    )
}

export default Category