import "../../Style/dashboard.css"

import { Table } from "react-bootstrap"
import SidebarComp from "../../Components/Sidebar/SidebarAdmin"
import { useEffect, useState } from "react"

import axios from "axios"

let apiURL = "https://fun-sparing.herokuapp.com"

const Venue = () => {

    const [fetchTrigger, setFetchTrigger] = useState(true);
    const [data, setData] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            const venue = await axios.get(`${apiURL}/venues`)
            const result = venue.data.venue
            setData(result)
        }
        if (fetchTrigger) {
            fetchData()
            setFetchTrigger(false)
        }
    }, [fetchTrigger])

    return (

        <div className="admin-content-container flex">
            <div className="dashboard-content-container flex-top">
                <SidebarComp />

                <div className="table-container margin-lrtb-percent3">
                    <div className="table-content-container">
                        <h3 className="inline-block">Venue Terdaftar</h3>

                        <br />
                        <br />

                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Venue</th>
                                    <th>Kategori</th>
                                    <th>Kota</th>
                                    <th>Provinsi</th>
                                    <th>Harga</th>
                                    <th>Fasilitas</th>
                                </tr>
                            </thead>
                            <tbody>
                                {data.map((item, index) => {
                                    return (
                                        <tr>
                                            <td>{index + 1}</td>
                                            <td>{item.venue_name}</td>
                                            <td>{item.SportCategory.category}</td>
                                            <td>{item.city}</td>
                                            <td>{item.province}</td>
                                            <td>{item.price}</td>
                                            <td>{item.facility}</td>

                                        </tr>
                                    )
                                })}


                            </tbody>
                        </Table>
                    </div>

                </div>
            </div>
        </div>


    )
}

export default Venue