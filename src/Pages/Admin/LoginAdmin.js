import '../../Style/authadmin.css';

import registerIcon from "../../Images/register.jpg"
import { Alert, Button, Form } from 'react-bootstrap';
import { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { AdminContext } from '../../Context/adminContext';

let apiURL = "https://fun-sparing.herokuapp.com"

const LoginAdmin = () => {
    const [, setAdmin] = useContext(AdminContext)
    const [input, setInput] = useState({ email: "", password: "" })
    const [error, setError] = useState()

    let navigate = useNavigate()

    const handleSubmit = async (event) => {
        event.preventDefault()
        await axios.post(`${apiURL}/admin/login`, input)
            .then((res) => {
                var token = res.data.token
                var currentAdmin = { token }
                setAdmin(currentAdmin)
                localStorage.clear()
                localStorage.setItem('admin', JSON.stringify(currentAdmin))
                navigate('/admin/dashboard')
            })
            .catch((err) => {
                setError(err.response.data.error)
            })
    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name
        setInput({ ...input, [name]: value })
    }

    return (
        <div className="content-container">
            <div className="margin-lrtb flex">
                <div className="image-container">
                    <img src={registerIcon} alt="register-icon" />
                </div>
                <div className="form-container padding-lrtb-percent">
                    {/* Form Component */}
                    <Form onSubmit={handleSubmit}>
                        <h1>Login Admin</h1>
                        <br></br>
                        {error !== undefined && (
                            <Alert key="danger" variant="danger">{error}</Alert>
                        )}

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control name="email" onChange={handleChange} type="email" placeholder="Masukkan Email" />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control name="password" onChange={handleChange} type="password" placeholder="Masukkan Password Minimum 6 Karakter" />
                        </Form.Group>

                        <br />
                        <Button variant="primary" type="submit">
                            Masuk
                        </Button>
                        <br />
                        <br />
                        <Form.Text className="text-muted">
                            Belum punya akun admin Fun Sparing ? <a href='/admin/register'>Daftar Sekarang</a>
                        </Form.Text>
                    </Form>

                </div>
            </div>
        </div>
    )
}

export default LoginAdmin;