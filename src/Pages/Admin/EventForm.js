import "../../Style/dashboard.css"

import { Alert, Button, Form } from "react-bootstrap"
import SidebarComp from "../../Components/Sidebar/SidebarAdmin"
import { useContext, useEffect, useState } from "react"

import { AdminContext } from "../../Context/adminContext"
import axios from "axios"
import { useNavigate } from "react-router-dom"

let apiURL = "https://fun-sparing.herokuapp.com"

const AddEventForm = () => {

    const initialInput = { event_name: "", host: "", city: "", province: "", fee: 0, email: "", phone: "", poster_url: "", description: "", sport_category_id: "", venue_id: "", time: "" }
    const [admin,] = useContext(AdminContext)
    const [input, setInput] = useState(initialInput)
    const [error, setError] = useState()

    const [category, setCategory] = useState([])
    const [venues, setVenues] = useState([])


    useEffect(() => {
        const fetchData = async () => {
            const category = await axios.get(`${apiURL}/sports/category`)
            const result = category.data.categories
            setCategory(result)

            const venue = await axios.get(`${apiURL}/venues`)
            const venueResult = venue.data.venue
            setVenues(venueResult)
        }
        fetchData()
    }, [])

    let navigate = useNavigate()

    const handleSubmit = async (event) => {
        event.preventDefault()
        await axios.post(`${apiURL}/events`, input, { headers: { Authorization: "Bearer " + admin.token } }
        )
            .then((res) => {
                navigate('/admin/events')
                setInput(initialInput)
            })
            .catch((err) => {
                setError(err.response.data.error)
            })
    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name
        if (name === "fee") {
            value = parseInt(value)
        } else if (name === "phone") {
            value = parseInt(value)
        }
        setInput({ ...input, [name]: value })
    }

    return (

        <div className="admin-content-container flex">
            <div className="dashboard-content-container flex-top">
                <SidebarComp />

                <div className="table-container margin-lrtb-percent3">
                    <div className="table-content-container">
                        <h3 className="inline-block">Event Olahraga</h3>
                        {error !== undefined && (
                            <Alert key="danger" variant="danger">{error}</Alert>
                        )}
                        <br />
                        <br />

                        <Form>
                            <Form.Group className="mb-4" >
                                <Form.Label>Nama Event</Form.Label>
                                <Form.Control name="event_name" onChange={handleChange} type="text" placeholder="Masukkan Nama Event" />
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Jenis Olahraga</Form.Label>
                                <Form.Select onChange={handleChange} name="sport_category_id" aria-label="Default select example">
                                    <option>Pilih Jenis Olahraga</option>
                                    {category.map((cat) => {
                                        return (
                                            <>
                                                <option value={cat.id}>{cat.category}</option>
                                            </>)
                                    })}
                                </Form.Select>
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Deskripsi Kegiatan</Form.Label>
                                <Form.Control name="description" onChange={handleChange} as="textarea" type="text" placeholder="Masukkan deskripsi kegiatan " />
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Penyelenggara</Form.Label>
                                <Form.Control name="host" onChange={handleChange} type="text" placeholder="Masukkan nama penyelenggara " />
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Kota</Form.Label>
                                <Form.Control name="city" onChange={handleChange} type="text" placeholder="Masukkan Nama Kota" />
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Provinsi</Form.Label>
                                <Form.Control name="province" onChange={handleChange} type="text" placeholder="Masukkan Nama Provinsi" />
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Tanggal Perlombaan</Form.Label>
                                <Form.Control name="time" onChange={handleChange} type="text" placeholder="Masukkan Tanggal Perlombaan, Format : YYYY-MM-DD" />
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Biaya Pendaftaran</Form.Label>
                                <Form.Control name="fee" onChange={handleChange} type="number" placeholder="Masukkan biaya pendaftaran" />
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Email Penyelenggara</Form.Label>
                                <Form.Control name="email" onChange={handleChange} type="text" placeholder="Masukkan Email Penyelenggara" />
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Nomor Telepon Penyelenggara</Form.Label>
                                <Form.Control name="phone" onChange={handleChange} type="number" placeholder="Masukkan Nomor Telepon Penyelenggara" />
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Venue Perlombaan</Form.Label>
                                <Form.Select onChange={handleChange} name="venue_id" aria-label="Default select example">
                                    <option>Pilih Venue Perlombaan</option>
                                    {venues.map((cat) => {
                                        return (
                                            <>
                                                <option value={cat.id}>{cat.venue_name}</option>
                                            </>)
                                    })}
                                </Form.Select>
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>URL Poster</Form.Label>
                                <Form.Control name="poster_url" onChange={handleChange} type="text" placeholder="Masukkan URL Poster" />
                            </Form.Group>

                            <Button onClick={handleSubmit} className="button-add-product" type="submit">
                                Tambah Event
                            </Button>
                        </Form>


                    </div>

                </div>
            </div>
        </div>


    )
}

export default AddEventForm