import '../../Style/authadmin.css';

import registerIcon from "../../Images/register.jpg"
import { Alert, Button, Form } from 'react-bootstrap';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

const RegisterAdmin = () => {

    const [input, setInput] = useState({ username: "", verification: "", fullname: "", email: "", password: "" })
    const [error, setError] = useState()

    let navigate = useNavigate()

    const handleSubmit = (event) => {
        event.preventDefault()
        if (input.verification === "1234verification4321") {
            axios.post("https://fun-sparing.herokuapp.com/admin/register", {
                username: input.username,
                fullname: input.fullname,
                email: input.email,
                password: input.password
            })
                .then((res) => {
                    navigate('/admin/login')
                })
                .catch((err) => {
                    setError(err.response.data.error)
                })
        } else {
            setError("Verification code is incorrect")
        }

    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name
        setInput({ ...input, [name]: value })
    }

    return (
        <div className="content-container">
            <div className="margin-lrtb flex">
                <div className="image-container">
                    <img src={registerIcon} alt="register-icon" />
                </div>
                <div className="form-container padding-lrtb-percent">
                    {/* Form Component */}
                    <Form onSubmit={handleSubmit}>
                        <h1>Register Admin</h1>
                        <br></br>
                        {error !== undefined && (
                            <Alert key="danger" variant="danger">{error}</Alert>
                        )}

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Username</Form.Label>
                            <Form.Control name="username" onChange={handleChange} type="text" placeholder="Masukkan Username" />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Nama Lengkap</Form.Label>
                            <Form.Control name="fullname" onChange={handleChange} type="text" placeholder="Masukkan Nama Lengkap" />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control name="email" onChange={handleChange} type="email" placeholder="Masukkan Email" />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control name="password" onChange={handleChange} type="password" placeholder="Masukkan Password Minimum 6 Karakter" />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Verifikasi Perusahaan</Form.Label>
                            <Form.Control name="verification" onChange={handleChange} type="password" placeholder="Masukkan Password Verifikasi" />
                        </Form.Group>

                        <br />
                        <Button variant="primary" type="submit">
                            Daftar
                        </Button>
                        <br />
                        <br />
                        <Form.Text className="text-muted">
                            Sudah punya akun admin Fun Sparing ? <a href='/admin/login'>Masuk Sekarang</a>
                        </Form.Text>
                    </Form>

                </div>
            </div>
        </div>
    )
}

export default RegisterAdmin;