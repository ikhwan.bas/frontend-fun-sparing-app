import "../../Style/dashboard.css"

import { Alert, Button, Form } from "react-bootstrap"
import SidebarComp from "../../Components/Sidebar/SidebarAdmin"
import { useContext, useEffect, useState } from "react"

import { AdminContext } from "../../Context/adminContext"
import axios from "axios"
import { useNavigate } from "react-router-dom"

let apiURL = "https://fun-sparing.herokuapp.com"

const AddVenueForm = () => {

    const initialInput = { venue_name: "", image_url: "", facility: "", city: "", province: "", price: 0, sport_category_id: "" }
    const [admin,] = useContext(AdminContext)
    const [input, setInput] = useState(initialInput)
    const [error, setError] = useState()

    const [category, setCategory] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            const category = await axios.get(`${apiURL}/sports/category`)
            const result = category.data.categories
            setCategory(result)
        }
        fetchData()
    }, [])

    let navigate = useNavigate()

    const handleSubmit = async (event) => {
        event.preventDefault()
        await axios.post(`${apiURL}/venues`, input, { headers: { Authorization: "Bearer " + admin.token } }
        )
            .then((res) => {
                navigate('/admin/venue')
                setInput(initialInput)
            })
            .catch((err) => {
                setError(err.response.data.error)
            })
    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name
        if (name === "price") {
            value = parseInt(value)
        }
        setInput({ ...input, [name]: value })
    }

    return (

        <div className="admin-content-container flex">
            <div className="dashboard-content-container flex-top">
                <SidebarComp />

                <div className="table-container margin-lrtb-percent3">
                    <div className="table-content-container">
                        <h3 className="inline-block">Venue Olahraga</h3>
                        {error !== undefined && (
                            <Alert key="danger" variant="danger">{error}</Alert>
                        )}
                        <br />
                        <br />

                        <Form>
                            <Form.Group className="mb-4" >
                                <Form.Label>Nama Venue</Form.Label>
                                <Form.Control name="venue_name" onChange={handleChange} type="text" placeholder="Masukkan Nama Venue" />
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Jenis Olahraga</Form.Label>
                                <Form.Select onChange={handleChange} name="sport_category_id" aria-label="Default select example">
                                    <option>Pilih Jenis Olahraga</option>
                                    {category.map((cat) => {
                                        return (
                                            <>
                                                <option value={cat.id}>{cat.category}</option>
                                            </>)
                                    })}
                                </Form.Select>
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Fasilitas</Form.Label>
                                <Form.Control name="facility" onChange={handleChange} as="textarea" type="text" placeholder="Masukkan fasilitas - fasilitas yang tersedia " />
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Kota</Form.Label>
                                <Form.Control name="city" onChange={handleChange} type="text" placeholder="Masukkan Nama Kota" />
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Provinsi</Form.Label>
                                <Form.Control name="province" onChange={handleChange} type="text" placeholder="Masukkan Nama Provinsi" />
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Harga Sewa per Jam</Form.Label>
                                <Form.Control name="price" onChange={handleChange} type="number" placeholder="Masukkan harga sewa per jam" />
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>URL Gambar</Form.Label>
                                <Form.Control name="image_url" onChange={handleChange} type="text" placeholder="Masukkan URL Gambar" />
                            </Form.Group>

                            <Button onClick={handleSubmit} className="button-add-product" type="submit">
                                Tambah Venue
                            </Button>
                        </Form>


                    </div>

                </div>
            </div>
        </div>


    )
}

export default AddVenueForm