import "../../Style/dashboard.css"
import SidebarComp from "../../Components/Sidebar/SidebarAdmin";

const DashboardAdmin = () => {


    return (
        <div className="admin-content-container flex">
            <div className="dashboard-content-container flex-top">
                <SidebarComp />

                <div className="table-container margin-lrtb-percent3">
                    <div className="table-content-container">

                        <h3>Semua Kategori</h3>
                        <br />

                    </div>

                </div>
            </div>
        </div>
    )
}

export default DashboardAdmin;