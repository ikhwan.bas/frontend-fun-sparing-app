import "../../Style/dashboard.css"

import { Table } from "react-bootstrap"
import SidebarComp from "../../Components/Sidebar/SidebarAdmin"
import { useEffect, useState } from "react"

import axios from "axios"

let apiURL = "https://fun-sparing.herokuapp.com"

const Events = () => {

    const [fetchTrigger, setFetchTrigger] = useState(true);
    const [data, setData] = useState([])
    useEffect(() => {
        const fetchData = async () => {
            const event = await axios.get(`${apiURL}/events`)
            const result = event.data.events
            setData(result)
        }
        if (fetchTrigger) {
            fetchData()
            setFetchTrigger(false)
        }
    }, [fetchTrigger])

    return (

        <div className="admin-content-container flex">
            <div className="dashboard-content-container flex-top">
                <SidebarComp />

                <div className="table-container margin-lrtb-percent3">
                    <div className="table-content-container">
                        <h3 className="inline-block">Event Berlangsung</h3>

                        <br />
                        <br />

                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Event</th>
                                    <th>Kategori</th>
                                    <th>Kota</th>
                                    <th>Provinsi</th>
                                    <th>Penyelenggara</th>
                                    <th>Biaya</th>

                                </tr>
                            </thead>
                            <tbody>
                                {data.map((item, index) => {
                                    return (
                                        <tr>
                                            <td>{index + 1}</td>
                                            <td>{item.time}</td>
                                            <td>{item.event_name}</td>
                                            <td>{item.SportCategory.category}</td>
                                            <td>{item.city}</td>
                                            <td>{item.province}</td>
                                            <td>{item.host}</td>
                                            <td>{item.fee}</td>

                                        </tr>
                                    )
                                })}


                            </tbody>
                        </Table>
                    </div>

                </div>
            </div>
        </div>


    )
}

export default Events