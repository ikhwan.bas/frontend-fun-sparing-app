import '../../Style/authadmin.css';

import iconclub from "../../Images/iconclubreg.jpg"
import { Alert, Button, Form } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

let apiURL = "https://fun-sparing.herokuapp.com"

const RegisterClub = () => {
    let initialInput = { club_name: "", email: "", password: "", phone: "", manager: "", city: "", province: "", logo_url: "", sport_category_id: "" }
    const [input, setInput] = useState(initialInput)
    const [error, setError] = useState()
    const [category, setCategory] = useState([])

    let navigate = useNavigate()

    useEffect(() => {
        const fetchData = async () => {
            const category = await axios.get(`${apiURL}/sports/category`)
            const result = category.data.categories
            setCategory(result)

        }
        fetchData()
    }, [])


    const handleSubmit = (event) => {
        event.preventDefault()
        axios.post("https://fun-sparing.herokuapp.com/club/register", input)
            .then((res) => {
                navigate('/club/login')
            })
            .catch((err) => {
                setError(err.response.data.error)
            })
    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name
        if (name === "phone") {
            value = parseInt(value)
        }
        setInput({ ...input, [name]: value })
    }

    return (
        <div className="content-container">
            <div className="margin-lrtb flex">
                <div className="image-container-club">
                    <img src={iconclub} alt="register-icon" />
                </div>
                <div className="form-container padding-lrtb-percent">
                    {/* Form Component */}
                    <Form onSubmit={handleSubmit}>
                        <h1>Register Club</h1>
                        <br></br>
                        {error !== undefined && (
                            <Alert key="danger" variant="danger">{error}</Alert>
                        )}

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Nama Club</Form.Label>
                            <Form.Control name="club_name" onChange={handleChange} type="text" placeholder="Masukkan Nama Club" />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email Club</Form.Label>
                            <Form.Control name="email" onChange={handleChange} type="email" placeholder="Masukkan Email Club" />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Password</Form.Label>
                            <Form.Control name="password" onChange={handleChange} type="password" placeholder="Masukkan password min 6 karakter" />
                            <Form.Text className="text-muted">
                                *minimum password 6 karakter
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Nama Manager</Form.Label>
                            <Form.Control name="manager" onChange={handleChange} type="text" placeholder="Masukkan Nama manager" />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Nomot Telepon Club</Form.Label>
                            <Form.Control name="phone" onChange={handleChange} type="text" placeholder="Masukkan Nomor Telepon Club" />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Kota</Form.Label>
                            <Form.Control name="city" onChange={handleChange} type="text" placeholder="Masukkan Nama Kota" />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Provinsi</Form.Label>
                            <Form.Control name="province" onChange={handleChange} type="text" placeholder="Masukkan Nama Provinsi" />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Logo URL</Form.Label>
                            <Form.Control name="logo_url" onChange={handleChange} type="text" placeholder="Masukkan URL Logo Club" />
                        </Form.Group>

                        <Form.Group className="mb-4">
                            <Form.Label>Jenis Olahraga</Form.Label>
                            <Form.Select onChange={handleChange} name="sport_category_id" aria-label="Default select example">
                                <option>Pilih Jenis Olahraga</option>
                                {category.map((cat) => {
                                    return (
                                        <>
                                            <option value={cat.id}>{cat.category}</option>
                                        </>)
                                })}
                            </Form.Select>
                        </Form.Group>

                        <br />
                        <Button variant="primary" type="submit">
                            Daftar
                        </Button>
                        <br />
                        <br />
                        <Form.Text className="text-muted">
                            Sudah punya akun club Fun Sparing ? <a href='/club/login'>Masuk Sekarang</a>
                        </Form.Text>
                    </Form>

                </div>
            </div>
        </div>
    )
}

export default RegisterClub;