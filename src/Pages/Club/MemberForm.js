import "../../Style/profile.css"

import { useContext, useState } from "react"

import axios from "axios"
import SidebarClub from "../../Components/Sidebar/SidebarClub"
import { ClubContext } from "../../Context/clubContext"
import { Alert, Button, Form } from "react-bootstrap"
import { useNavigate } from "react-router-dom"

let apiURL = "https://fun-sparing.herokuapp.com"

const ClubProfile = () => {
    let initialInput = { fullname: "", phone: 0, picture_url: "" }
    let navigate = useNavigate()

    const [club,] = useContext(ClubContext)

    const [input, setInput] = useState(initialInput)
    const [error, setError] = useState()

    const handleSubmit = async (event) => {
        event.preventDefault()
        await axios.post(`${apiURL}/club/member`, input, { headers: { Authorization: "Bearer " + club.token } }
        )
            .then((res) => {
                navigate('/club/dashboard')
                setInput(initialInput)
            })
            .catch((err) => {
                setError(err.response.data.error)
            })
    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name
        if (name === "phone") {
            value = parseInt(value)
        }
        setInput({ ...input, [name]: value })
    }

    return (
        <div className="club-content-container flex">
            <div className="dashboard-club-container flex-top">
                <SidebarClub />

                <div className="table-container margin-lrtb-percent3">
                    <div className="table-content-container">
                        {error !== undefined && (
                            <Alert key="danger" variant="danger">{error}</Alert>
                        )}
                        <h3 className="text-color ">Tambah Anggota Club</h3>
                        <br />

                        <Form>
                            <Form.Group className="mb-4" >
                                <Form.Label>Nama Anggota</Form.Label>
                                <Form.Control name="fullname" onChange={handleChange} type="text" placeholder="Masukkan Nama Lengkap Anggota" />
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Nomor Telepon Anggota</Form.Label>
                                <Form.Control name="phone" onChange={handleChange} type="number" placeholder="Masukkan Nomor Telepon Anggota" />
                            </Form.Group>

                            <Form.Group className="mb-4">
                                <Form.Label>Foto Profil Anggota</Form.Label>
                                <Form.Control name="picture_url" onChange={handleChange} type="text" placeholder="Masukkan URL Foto Profil" />
                            </Form.Group>

                            <Button onClick={handleSubmit} className="button-add-product" type="submit">
                                Tambah Anggota
                            </Button>
                        </Form>




                    </div>

                </div>
            </div>
        </div>


    )
}

export default ClubProfile