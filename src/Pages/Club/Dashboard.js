import "../../Style/dashboardClub.css"

import { useEffect, useState } from "react"

import axios from "axios"
import SidebarClub from "../../Components/Sidebar/SidebarClub"

let apiURL = "https://fun-sparing.herokuapp.com"

const ClubDashboard = () => {

    const [fetchTrigger, setFetchTrigger] = useState(true);
    const [, setData] = useState([])
    useEffect(() => {
        const fetchData = async () => {
            const event = await axios.get(`${apiURL}/events`)
            const result = event.data.events
            setData(result)
        }
        if (fetchTrigger) {
            fetchData()
            setFetchTrigger(false)
        }
    }, [fetchTrigger])

    return (

        <div className="club-content-container flex">
            <div className="dashboard-club-container flex-top">
                <SidebarClub />

                <div className="table-container margin-lrtb-percent3">
                    <div className="table-content-container">
                        <h3 className="inline-block text-color">Dashboard Club</h3>

                        <br />
                        <br />


                    </div>

                </div>
            </div>
        </div>


    )
}

export default ClubDashboard