import "../../Style/profile.css"

import { useContext, useEffect, useState } from "react"

import axios from "axios"
import SidebarClub from "../../Components/Sidebar/SidebarClub"
import { ClubContext } from "../../Context/clubContext"
import { Button, Table } from "react-bootstrap"

let apiURL = "https://fun-sparing.herokuapp.com"

const ClubProfile = () => {
    const [club,] = useContext(ClubContext)
    const [profile, setProfile] = useState([])
    const [member, setMember] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            const profile = await axios.get(`${apiURL}/club/profile`, { headers: { Authorization: "Bearer " + club.token } })
            setProfile(profile.data.data[0])
            setMember(profile.data.data[0].ClubMember)
        }
        fetchData()
    }, [club.token])

    return (
        <div className="club-content-container flex">
            <div className="dashboard-club-container flex-top">
                <SidebarClub />

                <div className="table-container margin-lrtb-percent3">
                    <div className="table-content-container">
                        <h3 className="inline-block text-color padding-lrtb-percent3">Profile Club</h3>

                        <div className="card-container flex-top padding-lrtb-percent3">
                            <div className="card-image-container ">
                                <img src={profile.logo_url} alt="logo_club" />
                            </div>
                            <div className="card-content-container">
                                <Table >
                                    <thead>
                                        <tr>
                                            <th>Nama Klub</th>
                                            <td>{profile.club_name}</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>{profile.email}</td>
                                        </tr>
                                        <tr>
                                            <th>Nomor Telepon</th>
                                            <td>{profile.phone}</td>
                                        </tr>
                                        <tr>
                                            <th>Kota</th>
                                            <td>{profile.city}</td>
                                        </tr>
                                        <tr>
                                            <th>Provinsi</th>
                                            <td>{profile.province}</td>
                                        </tr>
                                    </thead>

                                </Table>
                            </div>
                        </div>

                        <h3 className="inline-block text-color padding-lrtb-percent3">Anggota Club ({member.length})</h3>
                        {
                            member.map((member) => {
                                return (
                                    <div className="card-container flex-top padding-lrtb-percent3">
                                        <div className="card-image-container">
                                            <img src={member.picture_url} alt="logo_club" />
                                        </div>
                                        <div className="card-content-container">
                                            <Table >
                                                <thead>
                                                    <tr>
                                                        <th>Nama Lengkap</th>
                                                        <td>{member.fullname}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Nomor Telepon</th>
                                                        <td>{member.phone}</td>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <td>
                                                            <Button className="button-add-product" type="submit" variant="success">
                                                                Edit
                                                            </Button>
                                                            = or =
                                                            <Button className="button-add-product" type="submit" variant="danger">
                                                                Delete
                                                            </Button>
                                                        </td>
                                                    </tr>

                                                </thead>

                                            </Table>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>

                </div>
            </div>
        </div>


    )
}

export default ClubProfile