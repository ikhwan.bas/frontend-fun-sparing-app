import { useContext } from "react";
import { Container, Nav, Navbar, NavbarBrand } from "react-bootstrap";
import NavbarCollapse from "react-bootstrap/esm/NavbarCollapse";
import NavbarToggle from "react-bootstrap/esm/NavbarToggle";
import { useNavigate } from "react-router-dom";
import { AdminContext } from "../../Context/adminContext";
import { ClubContext } from "../../Context/clubContext";

const NavbarComp = () => {
    const [admin, setAdmin] = useContext(AdminContext);
    const [club, setClub] = useContext(ClubContext);

    let navigate = useNavigate();

    const authButton = () => {
        if (admin) {
            return (
                <>
                    <Nav.Link href="/admin/dashboard" className="text-white">Dashboard</Nav.Link>
                    <Nav.Link onClick={() => {
                        setAdmin(null)
                        localStorage.clear()
                        navigate('/')
                    }} className="text-white">Log Out</Nav.Link>
                </>
            )
        } else if (club) {
            return (
                <>
                    <Nav.Link href="/club/dashboard" className="text-white">Club Dashboard</Nav.Link>
                    <Nav.Link onClick={() => {
                        setClub(null)
                        localStorage.clear()
                        navigate('/')
                    }} className="text-white">Log Out</Nav.Link>
                </>
            )
        } else {
            return (
                <>
                    <Nav.Link href="/club/login" className="text-white">Login</Nav.Link>
                    <Nav.Link href="/club/register" className="text-white">Register</Nav.Link>
                </>
            )
        }
    }

    return (
        <Navbar collapseOnSelect expand="lg" className="navbar-background" >
            <Container>
                <NavbarBrand href="/" className="text-white">Fun Sparing</NavbarBrand>
                <NavbarToggle aria-controls="responsive-navbar-nav" />
                <NavbarCollapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link href="/" className="text-white">Beranda</Nav.Link>
                        <Nav.Link href="/events" className="text-white">Events</Nav.Link>
                        <Nav.Link href="/venue" className="text-white">Cari Lapangan</Nav.Link>
                        <Nav.Link href="/mabar" className="text-white">Main Bareng</Nav.Link>
                    </Nav>

                    <Nav>
                        {authButton()}
                    </Nav>
                </NavbarCollapse>
            </Container>
        </Navbar>
    )
}

export default NavbarComp;