import { Carousel } from "react-bootstrap"
import banner2 from "../../Images/banner-example2.jpg"
import banner3 from "../../Images/banner-example3.jpg"

const CarouselComp = () => {
    return (
        <div className="">
            <Carousel variant="dark">
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={banner2}
                        alt="Second slide"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={banner3}
                        alt="Third slide"
                    />
                </Carousel.Item>
            </Carousel>
        </div>

    )
}

export default CarouselComp