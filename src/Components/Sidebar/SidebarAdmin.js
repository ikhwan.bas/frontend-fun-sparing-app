import { Button } from "react-bootstrap";
import "../../Style/sidebar.css"

import { useNavigate } from "react-router-dom";
import { useContext } from "react";
import { AdminContext } from "../../Context/adminContext";

const SidebarComp = () => {
    const [, setAdmin] = useContext(AdminContext)
    let navigate = useNavigate()

    return (
        <div className="background-sidebar padding-lrtb-percent3">
            <h4>ADMIN DASHBOARD</h4>
            <hr />
            <div className="sidebar-menu">
                <h5>Kategori</h5>
                <ul>
                    <li>
                        <Button className="button-shape" name="category" onClick={() => { navigate('/admin/category') }} variant="light" size="sm">Semua Kategori</Button>
                    </li>
                </ul>

                <h5>Venue</h5>
                <ul>
                    <li>
                        <Button className="button-shape" name="venue" onClick={() => { navigate('/admin/venue') }} variant="light" size="sm">Semua Venue</Button>
                    </li>
                    <li>
                        <Button className="button-shape" name="addvenue" onClick={() => { navigate('/admin/venue/form') }} variant="light" size="sm">Tambah Venue</Button>
                    </li>
                </ul>

                <h5>Event</h5>
                <ul>
                    <li>
                        <Button className="button-shape" name="dashboard" onClick={() => { navigate('/admin/events') }} variant="light" size="sm">Semua Event</Button>
                    </li>
                    <li>
                        <Button className="button-shape" name="dashboard" onClick={() => { navigate('/admin/events/form') }} variant="light" size="sm">Tambah Event</Button>
                    </li>
                </ul>

                <h5>Akun</h5>
                <ul>
                    <li>
                        <Button className="button-shape" name="dashboard" onClick={() => {
                            setAdmin(null)
                            localStorage.clear()
                            navigate('/')
                        }} variant="light" size="sm">Log Out</Button>
                    </li>
                </ul>


            </div>
        </div>
    )
}

export default SidebarComp;