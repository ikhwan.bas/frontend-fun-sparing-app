import { Button } from "react-bootstrap";
import "../../Style/sidebar.css"

import { useNavigate } from "react-router-dom";
import { useContext } from "react";
import { ClubContext } from "../../Context/clubContext";

const SidebarClub = () => {
    const [, setClub] = useContext(ClubContext)
    let navigate = useNavigate()

    return (
        <div className="background-sidebar padding-lrtb-percent3">
            <h4 className="display-center">CLUB DASHBOARD</h4>
            <hr />
            <div className="sidebar-menu-club">
                <h5>Profil Club</h5>
                <ul>
                    <li>
                        <Button className="button-shape" name="category" onClick={() => { navigate('/club/profile') }} variant="light" size="sm">Profil Club</Button>
                    </li>
                    <li>
                        <Button className="button-shape" name="category" onClick={() => { navigate('/club/member/form') }} variant="light" size="sm">+ Anggota</Button>
                    </li>
                </ul>

                <h5>Akun</h5>
                <ul>
                    <li>
                        <Button className="button-shape" name="dashboard" onClick={() => {
                            setClub(null)
                            localStorage.clear()
                            navigate('/')
                        }} variant="light" size="sm">Log Out</Button>
                    </li>
                </ul>


            </div>
        </div>
    )
}

export default SidebarClub;