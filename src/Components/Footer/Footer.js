const FooterComponent = () => {
    return (
        <div className="footer-container display-center ">
            <h6>Copyright @ Fun Sparing 2022</h6>
        </div>
    )
}

export default FooterComponent;