import { useContext } from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import FooterComponent from '../Components/Footer/Footer';
import NavbarComp from '../Components/Navbar/Navbar';
import { AdminContext } from '../Context/adminContext';
import { ClubContext } from '../Context/clubContext';
import Category from '../Pages/Admin/Category';
import DashboardAdmin from '../Pages/Admin/Dashboard';
import Events from '../Pages/Admin/Event';
import AddEventForm from '../Pages/Admin/EventForm';
import LoginAdmin from '../Pages/Admin/LoginAdmin';
import RegisterAdmin from '../Pages/Admin/RegisterAdmin';
import Venue from '../Pages/Admin/Venue';
import AddVenueForm from '../Pages/Admin/VenueForm';
import ClubDashboard from '../Pages/Club/Dashboard';
import LoginClub from '../Pages/Club/LoginClub';
import MemberForm from '../Pages/Club/MemberForm';
import ClubProfile from '../Pages/Club/Profile';
import RegisterClub from '../Pages/Club/RegisterClub';
import EventPage from '../Pages/Public/Event';
import EventDetail from '../Pages/Public/EventDetail';
import Homepage from '../Pages/Public/Homepage';
import MabarPage from '../Pages/Public/Mabar';
import VenuePage from '../Pages/Public/Venue';
import VenueDetail from '../Pages/Public/VenueDetail';

const AppRoutes = () => {
  const [admin,] = useContext(AdminContext)
  const [club,] = useContext(ClubContext)

  const AdminRoute = ({ children }) => {
    if (!admin) {
      return <Navigate to="/admin/login" />;
    } else {
      return children
    }
  };

  const ClubRoute = ({ children }) => {
    if (!club) {
      return <Navigate to="/club/login" />;
    } else {
      return children
    }
  };

  return (
    <BrowserRouter>
      <Routes>
        {/* Admin Routes */}
        <Route path="/admin/dashboard" element={
          <AdminRoute>
            <DashboardAdmin />
          </AdminRoute>
        } />
        <Route path="/admin/category" element={
          <AdminRoute>
            <Category />
          </AdminRoute>
        } />
        <Route path="/admin/venue" element={
          <AdminRoute>
            <Venue />
          </AdminRoute>
        } />

        <Route path="/admin/venue/form" element={
          <AdminRoute>
            <AddVenueForm />
          </AdminRoute>
        } />
        <Route path="/admin/events" element={
          <AdminRoute>
            <Events />
          </AdminRoute>
        } />
        <Route path="/admin/events/form" element={
          <AdminRoute>
            <AddEventForm />
          </AdminRoute>
        } />

        <Route path="/admin/register" element={
          <>
            <NavbarComp />
            <RegisterAdmin />
            <FooterComponent />
          </>
        } />
        <Route path="/admin/login" element={
          <>
            <NavbarComp />
            <LoginAdmin />
            <FooterComponent />
          </>
        } />

        {/* Club Routes */}
        <Route path="/club/register" element={
          <>
            <NavbarComp />
            <RegisterClub />
            <FooterComponent />
          </>
        } />
        <Route path="/club/login" element={
          <>
            <NavbarComp />
            <LoginClub />
            <FooterComponent />
          </>
        } />

        <Route path="/club/dashboard" element={
          <ClubRoute>
            <NavbarComp />
            <ClubDashboard />
          </ClubRoute>
        } />

        <Route path="/club/profile" element={
          <ClubRoute>
            <NavbarComp />
            <ClubProfile />
          </ClubRoute>
        } />

        <Route path="/club/member/form" element={
          <ClubRoute>
            <NavbarComp />
            <MemberForm />
          </ClubRoute>
        } />

        {/* public Routes */}
        <Route path="/" element={
          <>
            <NavbarComp />
            <Homepage />
            <FooterComponent />
          </>
        } />

        <Route path="/events" element={
          <>
            <NavbarComp />
            <EventPage />
            <FooterComponent />
          </>
        } />

        <Route path="/event/:id" element={
          <>
            <NavbarComp />
            <EventDetail />
            <FooterComponent />
          </>
        } />

        <Route path="/venue" element={
          <>
            <NavbarComp />
            <VenuePage />
            <FooterComponent />
          </>
        } />

        <Route path="/venue/:id" element={
          <>
            <NavbarComp />
            <VenueDetail />
            <FooterComponent />
          </>
        } />

        <Route path="/mabar" element={
          <>
            <NavbarComp />
            <MabarPage />
            <FooterComponent />
          </>
        } />

        {/* If false url return to homepage */}
        <Route path="*" element={<Navigate to="/" replace />} />

      </Routes>
    </BrowserRouter>
  );
}

export default AppRoutes;
